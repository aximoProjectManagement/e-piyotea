<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\MemberController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('login', function () {
    return view('login');
})->name('login');
Route::post('login',[\App\Http\Controllers\AuthController::class,'login']);
Route::group(['prefix' => 'admin',  'middleware' => 'auth'], function()
{
    //All the routes that belongs to the group goes here
    Route::get('',[AdminController::class,'dashboard']);
    Route::get('adminList',[AdminController::class,'adminList']);
    Route::get('add',[AdminController::class,'add']);
    Route::post('save_admin',[AdminController::class,'save_admin']);

    Route::get('edit/{id}',[AdminController::class,'edit_admin']);
    Route::get('delete_admin/{id}',[AdminController::class,'delete_admin']);
    Route::post('update_admin',[AdminController::class,'update_admin']);


    Route::get('memberList',[AdminController::class,'memberList']);
    Route::get('add_member',[AdminController::class,'add_member']);
    Route::post('save_member',[AdminController::class,'save_member']);
    Route::get('order',[AdminController::class,'orders']);

    Route::get('edit_member/{id}',[AdminController::class,'edit_member']);
    Route::get('delete_member/{id}',[AdminController::class,'delete_member']);
    Route::post('update_member',[AdminController::class,'update_member']);
});
Route::group(['prefix' => 'member',  'middleware' => 'auth'], function()
{
    //All the routes that belongs to the group goes here
    Route::get('',[MemberController::class,'dashboard']);
});



