<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin')}}">
                <i class="mdi mdi-grid-large menu-icon"></i>
                <span class="menu-title">HOME</span>
            </a>
        </li>

        <li class="nav-item nav-category">EPIYOTEA</li>
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" href="#">--}}
{{--                <i class="menu-icon mdi mdi-account-circle-outline"></i>--}}
{{--                <span class="menu-title">Profile</span>--}}
{{--            </a>--}}
{{--        </li>--}}
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/adminList')}}">
                <i class="menu-icon mdi mdi-card-text-outline"></i>
                <span class="menu-title">Admin</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('admin/memberList')}}">
                <i class="menu-icon mdi mdi-table"></i>
                <span class="menu-title">Member</span>
            </a>
        </li>

        <li class="nav-item">
            <a id="orrder" class="nav-link" data-bs-toggle="collapse" href="{{url('admin/order')}}" aria-expanded="false" aria-controls="icons">
                <i class="menu-icon mdi mdi-layers-outline"></i>
                <span class="menu-title">Order</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="icons">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{url('admin/order')}}">Active Order</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('admin/order')}}">Pending Order</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('admin/order')}}">Delivered</a></li>
                </ul>
            </div>
        </li>
        <!-- <li class="nav-item nav-category">pages</li>
        <li class="nav-item">
          <a class="nav-link" data-bs-toggle="collapse" href="#auth" aria-expanded="false" aria-controls="auth">
            <i class="menu-icon mdi mdi-account-circle-outline"></i>
            <span class="menu-title">User Pages</span>
            <i class="menu-arrow"></i>
          </a>
          <div class="collapse" id="auth">
            <ul class="nav flex-column sub-menu">
              <li class="nav-item"> <a class="nav-link" href="pages/samples/login.html"> Login </a></li>
            </ul>
          </div>
        </li> -->
        <!-- <li class="nav-item nav-category">help</li>
        <li class="nav-item">
          <a class="nav-link" href="http://bootstrapdash.com/demo/star-admin2-free/docs/documentation.html">
            <i class="menu-icon mdi mdi-file-document"></i>
            <span class="menu-title">Documentation</span>
          </a>
        </li> -->
    </ul>
</nav>
<script>
    $("#orrder").click( function() {
        $("#icons").show();
       // alert('click');
    });
</script>
