<!DOCTYPE html>
<html lang="en">
@include('admin.layout.head')
<body>
<div class="container-scroller">

    <!-- partial:partials/_navbar.html -->
    @include('admin.layout.header')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->


        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
      @include('admin.layout.sidebar')
        <!-- table -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="home-tab">
                            <div class="d-sm-flex align-items-center justify-content-between border-bottom">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="true">EPIYOTEA</a>
                                    </li>

                                </ul>
                                <div>
                                    <div class="btn-wrapper">
                                        <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Share</a>
                                        <a href="#" class="btn btn-otline-dark"><i class="icon-printer"></i> Print</a>
                                        <a href="#" class="btn btn-primary text-white me-0"><i class="icon-download"></i> Export</a>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content tab-content-basic">
                                <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="overview">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="statistics-details d-flex align-items-center justify-content-between">
                                                <div>
                                                    <p class="statistics-title">Sale</p>
                                                    <h3 class="rate-percentage">32.53%</h3>
                                                    <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>-0.5%</span></p>
                                                </div>
                                                <div>
                                                    <p class="statistics-title">Visitor</p>
                                                    <h3 class="rate-percentage">7,682</h3>
                                                    <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>+0.1%</span></p>
                                                </div>
                                                <div>
                                                    <p class="statistics-title">New order</p>
                                                    <h3 class="rate-percentage">68.8</h3>
                                                    <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>68.8</span></p>
                                                </div>
                                                <div class="d-none d-md-block">
                                                    <p class="statistics-title">happy customer</p>
                                                    <h3 class="rate-percentage">2m:35s</h3>
                                                    <p class="text-success d-flex"><i class="mdi mdi-menu-down"></i><span>+0.8%</span></p>
                                                </div>
                                                <div class="d-none d-md-block">
                                                    <p class="statistics-title">New members</p>
                                                    <h3 class="rate-percentage">68.8</h3>
                                                    <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>68.8</span></p>
                                                </div>
                                                <div class="d-none d-md-block">
                                                    <p class="statistics-title">upcoming products</p>
                                                    <h3 class="rate-percentage">2m:35s</h3>
                                                    <p class="text-success d-flex"><i class="mdi mdi-menu-down"></i><span>+0.8%</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8 d-flex flex-column">
                                            <div class="row flex-grow">
                                                <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
                                                    <div class="card card-rounded">
                                                        <div class="card-body">
                                                            <div class="d-sm-flex justify-content-between align-items-start">
                                                                <div>
                                                                    <h4 class="card-title card-title-dash">REVENUE</h4>
                                                                    <!-- <h5 class="card-subtitle card-subtitle-dash">Lorem Ipsum is simply dummy text of the printing</h5> -->
                                                                </div>
                                                                <div id="performance-line-legend"><div class="chartjs-legend"><ul><li><span style="background-color:#1F3BB3"></span>This week</li><li><span style="background-color:#52CDFF"></span>Last week</li></ul></div></div>
                                                            </div>
                                                            <div class="chartjs-wrapper mt-5"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                                                <canvas id="performaneLine" style="display: block; height: 150px; width: 599px;" width="898" height="225" class="chartjs-render-monitor"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 d-flex flex-column">
                                            <div class="row flex-grow">
                                                <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                                                    <div class="card card-rounded">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <div class="d-flex justify-content-between align-items-center mb-2 mb-sm-0">
                                                                        <div class="circle-progress-width">
                                                                            <div id="totalVisitors" class="progressbar-js-circle pr-2"><svg viewBox="0 0 100 100" style="display: block; width: 100%;"><path d="M 50,50 m 0,-42.5 a 42.5,42.5 0 1 1 0,85 a 42.5,42.5 0 1 1 0,-85" stroke="#eee" stroke-width="15" fill-opacity="0"></path><path d="M 50,50 m 0,-42.5 a 42.5,42.5 0 1 1 0,85 a 42.5,42.5 0 1 1 0,-85" stroke="rgb(99,137,233)" stroke-width="15" fill-opacity="0" style="stroke-dasharray: 267.132, 267.132; stroke-dashoffset: 96.1674;"></path></svg><div class="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(255, 255, 255); font-size: 0rem;">64</div></div>
                                                                        </div>
                                                                        <div>
                                                                            <p class="text-small mb-2">Active Order</p>
                                                                            <h4 class="mb-0 fw-bold">26.80%</h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="d-flex justify-content-between align-items-center">
                                                                        <div class="circle-progress-width">
                                                                            <div id="visitperday" class="progressbar-js-circle pr-2"><svg viewBox="0 0 100 100" style="display: block; width: 100%;"><path d="M 50,50 m 0,-42.5 a 42.5,42.5 0 1 1 0,85 a 42.5,42.5 0 1 1 0,-85" stroke="#eee" stroke-width="15" fill-opacity="0"></path><path d="M 50,50 m 0,-42.5 a 42.5,42.5 0 1 1 0,85 a 42.5,42.5 0 1 1 0,-85" stroke="rgb(60,168,179)" stroke-width="15" fill-opacity="0" style="stroke-dasharray: 267.132, 267.132; stroke-dashoffset: 176.307;"></path></svg><div class="progressbar-text" style="position: absolute; left: 50%; top: 50%; padding: 0px; margin: 0px; transform: translate(-50%, -50%); color: rgb(255, 255, 255); font-size: 0rem;">34</div></div>
                                                                        </div>
                                                                        <div>
                                                                            <p class="text-small mb-2">Pending Order</p>
                                                                            <h4 class="mb-0 fw-bold">9065</h4>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-12 grid-margin stretch-card">
                                                    <div class="card bg-primary card-rounded">
                                                        <div class="card-body pb-0">
                                                            <h4 class="card-title card-title-dash text-white mb-4">Status Summary</h4>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <p class="status-summary-ight-white mb-1">Closed Value</p>
                                                                    <h2 class="text-info">357</h2>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="status-summary-chart-wrapper pb-4"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                                                        <canvas id="status-summary" width="244" height="99" style="display: block; height: 66px; width: 163px;" class="chartjs-render-monitor"></canvas>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row flex-grow">
                                        <div class="col-12 grid-margin stretch-card">
                                            <div class="card card-rounded">
                                                <div class="card-body">
                                                    <div class="d-sm-flex justify-content-between align-items-start">
                                                        <div>
                                                            <h4 class="card-title card-title-dash">New Members</h4>
                                                            <p class="card-subtitle card-subtitle-dash">You have 50+ new members</p>
                                                        </div>
                                                        <div>
                                                            <button class="btn btn-primary btn-lg text-white mb-0 me-0" type="button"><i class="mdi mdi-account-plus"></i>Add new member</button>
                                                        </div>
                                                    </div>
                                                    <div class="table-responsive  mt-1">
                                                        <table class="table select-table">
                                                            <thead>
                                                            <tr>
                                                                <th>
                                                                    <div class="form-check form-check-flat mt-0">
                                                                        <label class="form-check-label">
                                                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i><i class="input-helper"></i></label>
                                                                    </div>
                                                                </th>
                                                                <th>Member</th>
                                                                <th>Email ID</th>
                                                                <th>Phone</th>
                                                                <th>State</th>

                                                                <th>Edit</th>
                                                                <th>Delete</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>
                                                                    <div class="form-check form-check-flat mt-0">
                                                                        <label class="form-check-label">
                                                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i><i class="input-helper"></i></label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="d-flex ">
                                                                        <img src="images/faces/face1.jpg" alt="">
                                                                        <div>
                                                                            <h6>Brandon Washington</h6>
                                                                            <p>Head admin</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <h6>info@aximoinfotech.com</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>9897636633</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>Delhi</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-info">Edit</div>
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-danger deshbd-mmbr">Delete</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="form-check form-check-flat mt-0">
                                                                        <label class="form-check-label">
                                                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i><i class="input-helper"></i></label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="d-flex">
                                                                        <img src="images/faces/face2.jpg" alt="">
                                                                        <div>
                                                                            <h6>Laura Brooks</h6>
                                                                            <p>Head admin</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <h6>info@aximoinfotech.com</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>9897636633</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>Delhi</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-info">Edit</div>
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-danger deshbd-mmbr">Delete</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="form-check form-check-flat mt-0">
                                                                        <label class="form-check-label">
                                                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i><i class="input-helper"></i></label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="d-flex">
                                                                        <img src="images/faces/face3.jpg" alt="">
                                                                        <div>
                                                                            <h6>Wayne Murphy</h6>
                                                                            <p>Head admin</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <h6>info@aximoinfotech.com</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>9897636633</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>Delhi</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-info">Edit</div>
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-danger deshbd-mmbr">Delete</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="form-check form-check-flat mt-0">
                                                                        <label class="form-check-label">
                                                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i><i class="input-helper"></i></label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="d-flex">
                                                                        <img src="images/faces/face4.jpg" alt="">
                                                                        <div>
                                                                            <h6>Matthew Bailey</h6>
                                                                            <p>Head admin</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <h6>info@aximoinfotech.com</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>9897636633</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>Delhi</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-info">Edit</div>
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-danger deshbd-mmbr">Delete</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div class="form-check form-check-flat mt-0">
                                                                        <label class="form-check-label">
                                                                            <input type="checkbox" class="form-check-input" aria-checked="false"><i class="input-helper"></i><i class="input-helper"></i></label>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div class="d-flex">
                                                                        <img src="images/faces/face5.jpg" alt="">
                                                                        <div>
                                                                            <h6>Katherine Butler</h6>
                                                                            <p>Head admin</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <h6>info@aximoinfotech.com</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>9897636633</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <h6>Delhi</h6>
                                                                    <!-- <p>company type</p> -->
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-info">Edit</div>
                                                                </td>
                                                                <td>
                                                                    <div class="btn btn-danger deshbd-mmbr">Delete</div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- table -->

        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- plugins:js -->
    <script src="vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="vendors/chart.js/Chart.min.js"></script>
    <script src="vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendors/progressbar.js/progressbar.min.js"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="js/off-canvas.js"></script>
    <script src="js/hoverable-collapse.js"></script>
    <script src="js/template.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/todolist.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="js/jquery.cookie.js" type="text/javascript"></script>
    <script src="js/dashboard.js"></script>
    <script src="js/Chart.roundedBarCharts.js"></script>
    <!-- End custom js for this page-->
</body>


<!-- Mirrored from www.bootstrapdash.com/demo/star-admin2-free/template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Oct 2021 11:59:33 GMT -->
</html>

