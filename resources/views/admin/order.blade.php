<!DOCTYPE html>
<html lang="en">
@include('admin.layout.head')
<body>
<div class="container-scroller">

    <!-- partial:partials/_navbar.html -->
    @include('admin.layout.header')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->


        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
      @include('admin.layout.sidebar')
        <!-- table -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    @if(Session::has('msg'))
                        <p class="alert alert-info">{{ Session::get('msg') }}</p>
                    @endif
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-sm-flex justify-content-between align-items-start">
                                    <div>
                                        <h4 class="card-title card-title-dash">Orders</h4>
                                        <p class="card-subtitle card-subtitle-dash">You have 50+ new orders</p>
                                    </div>
{{--                                    <div>--}}
{{--                                        <a href="{{url('admin/add')}}" class="btn btn-primary  text-white mb-0 me-0" type="button"><i class="mdi mdi-account-plus"></i>Add new member</a>--}}
{{--                                    </div>--}}
                                </div>
                                <div class="table-responsive  mt-1">
                                    <table class="table select-table">
                                        <thead>
                                        <tr>

                                            <th>Order ID</th>
                                            <th>Customers Name</th>
                                            <th>Mobile No.</th>
                                            <th>Product</th>
                                            <th>Quantity</th>
                                            <th>State</th>
                                            <th>City</th>
                                            <th>Address</th>
                                            <th>Payment Status</th>
                                            <th>Order Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orders as $order)
                                        <tr>

                                            <td>
                                                <h6>{{$order->id}}</h6>
                                            </td>
                                            <td>
                                                <h6>{{$order->customer_name}}</h6>
                                            </td>
                                            <td>
                                                <h6>{{$order->mobile}}</h6>
                                                <!-- <p>company type</p> -->
                                            </td>
                                            <td>
                                                <h6>{{$order->product}}</h6>
                                                <!-- <p>company type</p> -->
                                            </td>
                                            <td>
                                                <h6>{{$order->quantity}}</h6>
                                                <!-- <p>company type</p> -->
                                            </td>

                                            <td>
                                                <h6>{{$order->state}}</h6>
                                                <!-- <p>company type</p> -->
                                            </td>
                                            <td>
                                                <h6>{{$order->city}}</h6>
                                                <!-- <p>company type</p> -->
                                            </td>
                                            <td>
                                                <h6>{{$order->address}}</h6>
                                                <!-- <p>company type</p> -->
                                            </td>
                                            <td>
                                                <h6>{{$order->payment_status}}</h6>
                                                <!-- <p>company type</p> -->
                                            </td>
                                            <td>

                                                <a href="{{url('order/status_change/'.$order->id)}}" class="btn btn-sm btn-info"><h6>{{$order->payment_status}}</h6></a>
                                                <!-- <p>company type</p> -->
                                            </td>

                                            <td>
                                                <a href="href="{{url('order/delete/'.$order->id)}}" class="btn btn-sm btn-danger deshbd-mmbr">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <!-- table -->
            <!-- partial -->

            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- plugins:js -->
    <script src="vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="vendors/chart.js/Chart.min.js"></script>
    <script src="vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <script src="vendors/progressbar.js/progressbar.min.js"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="js/off-canvas.js"></script>
    <script src="js/hoverable-collapse.js"></script>
    <script src="js/template.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/todolist.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="js/jquery.cookie.js" type="text/javascript"></script>
    <script src="js/dashboard.js"></script>
    <script src="js/Chart.roundedBarCharts.js"></script>
    <!-- End custom js for this page-->
</body>


<!-- Mirrored from www.bootstrapdash.com/demo/star-admin2-free/template/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 04 Oct 2021 11:59:33 GMT -->
</html>

