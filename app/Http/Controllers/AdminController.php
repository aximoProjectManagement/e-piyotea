<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;

class AdminController extends Controller
{
    public function dashboard()
    {
       return view('admin.dashboard');
    }

    public function adminList()
    {
        $admins = DB::table('users')->where('role','admin')->get();
        return view('admin.admin.list',compact('admins'));
    }
    public function add()
    {
        return view('admin.admin.add');
    }

    public function delete_admin($id)
    {
        DB::table('users')->where('id',base64_decode($id))->delete();
        Session::flash('msg','Admin deleted Successfully');
        return back();
    }
    public function delete_member($id)
    {
        DB::table('users')->where('id',base64_decode($id))->delete();
        Session::flash('msg','Member deleted Successfully');
        return back();
    }

    public function save_admin(Request $request)
    {
        //dd($request->all());
        if ($request->hasFile('admin_image')){
            $request->validate([
                'admin_image' => 'required|image|mimes:jpeg,png,jpg,gif',
            ]);
            $image = $request->file('admin_image');
            $imageName = $image->getClientOriginalName();


            $image->move(public_path().'/images/', $imageName);
        }
        $insert = DB::table('users')->insert([
            'name'=>$request->name,
            'email'=>$request->email,
            'mobile'=>$request->mobile,
            'password'=>Hash::make($request->password),
            'state'=>$request->state,
            'city'=>$request->city,
            'image'=>public_path().'/images/'. $imageName,
            'role'=>'admin',
        ]);
        if ($insert == true){
            Session::flash('msg','Admin Added Successfully');
            return redirect('admin/adminList');
        }

    }

    public function add_member()
    {
        return view('admin.member.add');
    }

    public function edit_admin($id)
    {
        $admin = DB::table('users')->where('id',base64_decode($id))->first();
        return view('admin.admin.edit',compact('admin'));
    }

    public function edit_member($id)
    {
        $member = DB::table('users')->where('id',base64_decode($id))->first();
        return view('admin.member.edit',compact('member'));
    }

    public function save_member(Request $request)
    {
        if ($request->hasFile('admin_image')){
            $request->validate([
                'admin_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $image = $request->file('admin_image');
            $imageName = $image->getClientOriginalName();

            //$imageName = time().'.'.$request->admin_image->extension();

            $image->move(public_path().'/images/', $imageName);
        }
        $insert = DB::table('users')->insert([
            'name'=>$request->name,
            'email'=>$request->email,
            'mobile'=>$request->mobile,
            'password'=>Hash::make($request->password),
            'state'=>$request->state,
            'city'=>$request->city,
            'image'=>public_path().'/images/'. $imageName,
            'role'=>'member',
            'u_id'=>Auth::id(),
        ]);
        if ($insert == true){
            Session::flash('msg','Member Added Successfully');
            return redirect('admin/memberList');
        }

    }

    public function memberList()
    {
        $admins = DB::table('users')->where('role','member')->get();
        return view('admin.member.list',compact('admins'));
    }

    public function update_admin(Request $request)
    {
        if ($request->hasFile('admin_image')){
            $request->validate([
                'admin_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $image = $request->file('admin_image');
            $imageName = $image->getClientOriginalName();


            $image->move(public_path().'/images/', $imageName);
        }else{
            $imageName = $request->okdimg;
        }
        $insert = DB::table('users')->where('id',$request->id)->update([
            'name'=>$request->name,
            'mobile'=>$request->mobile,
            'state'=>$request->state,
            'city'=>$request->city,
            'image'=>public_path().'/images/'. $imageName,
            'role'=>'admin',
        ]);
        if ($insert == true){
            Session::flash('msg','admin update Successfully');
            return redirect('admin/adminList');
        }

    }
    public function update_member(Request $request)
    {
        //dd($request->all());
        if ($request->hasFile('member_image')){
            $request->validate([
                'member_image' => 'required|image|mimes:jpeg,png,jpg,gif',
            ]);
            $image = $request->file('member_image');
            $imageName = $image->getClientOriginalName();


            $image->move(public_path().'/images/', $imageName);
        }else{
            $imageName = $request->okdimg;
        }
        $insert = DB::table('users')->where('id',$request->id)->update([
            'name'=>$request->name,
            'mobile'=>$request->mobile,
            'state'=>$request->state,
            'city'=>$request->city,
            'image'=>public_path().'/images/'. $imageName,
            'role'=>'member',
            'u_id'=>Auth::id(),
        ]);
        if ($insert == true){
            Session::flash('msg','member update Successfully');
            return redirect('admin/memberList');
        }

    }


    public function orders()
    {
        $orders = DB::table('orders')->get();
        return view('admin.order',compact('orders'));
    }
}
