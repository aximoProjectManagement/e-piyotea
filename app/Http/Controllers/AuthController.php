<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            if (Auth::user()->role =='admin' || Auth::user()->role =='superadmin'){
                return redirect()->intended('admin');
            }else{
                return redirect()->intended('member');
            }

        }else{
            return back();
        }
    }
}
