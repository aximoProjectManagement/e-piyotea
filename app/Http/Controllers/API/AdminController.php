<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    //
    public function getAllMember()
    {
        $member = DB::table('users')->where('role','member')->get();
        if ($member){
            return ['status'=> true, 'message'=>'All members successfully fetched!', 'result'=> $member];
        }else{
            return ['status'=> false, 'message'=>'No members available!', 'result'=> null];
        }
    }
    public function getTeamMember($id)
    {
        $member = DB::table('users')->where('u_id',$id)->where('role','member')->get();
        if ($member){
            return ['status'=> true, 'message'=>'All team members successfully fetched!', 'result'=> $member];
        }else{
            return ['status'=> false, 'message'=>'No members available!', 'result'=> null];
        }
    }

    public function addTeamMember(Request $request)
    {
        //dd($request->all());
        if ($request->hasFile('member_image')){
            $request->validate([
                'member_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $image = $request->file('member_image');
            $imageName = $image->getClientOriginalName();

            //$imageName = time().'.'.$request->admin_image->extension();

            $image->move(public_path().'/images/', $imageName);
        }

        if ($request->hasFile('aadhar_image')){
            $request->validate([
                'aadhar_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $image = $request->file('aadhar_image');
            $imageName2 = $image->getClientOriginalName();

            //$imageName = time().'.'.$request->admin_image->extension();

            $image->move(public_path().'/images/', $imageName2);
        }
        $insert = DB::table('users')->insert([
            'name'=>$request->name,
            'email'=>$request->email,
            'mobile'=>$request->mobile,
            'password'=>Hash::make($request->password),
            'country'=>$request->country,
            'state'=>$request->state,
            'city'=>$request->district,
            'aadhar_number'=>$request->aadhar_number,
            'pan_number'=>$request->pan_number,
            'image'=>public_path().'/images/'. $imageName,
            'aadhar_image'=>public_path().'/images/'. $imageName2,
            'role'=>'member',
            'u_id'=>$request->uid,
            'ref_id'=>$request->reference_id,
            'dob'=>$request->dateofbirth,
            'gender'=>$request->gender,
        ]);
        if ($insert == true){
            return ['status'=> true, 'message'=>'member added successfully!', 'result'=> null];
        }else{
            return ['status'=> false, 'message'=>'something went wrong!', 'result'=> null];
        }


    }
}
