<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    //
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $result = Auth::user();
            $order = DB::table('orders')->get();
            $result->total_orders = count($order);
            // Authentication passed...
            return ['status'=> true, 'message'=>'Login success!', 'result'=> $result];

        }else{
            return ['status'=> false, 'message'=>'Invalid credentials', 'result'=> null];
        }
    }
}
